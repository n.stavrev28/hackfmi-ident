"use strict";
var IdentUserPermission = (function () {
    function IdentUserPermission() {
    }
    IdentUserPermission.PATIENT = "patient";
    IdentUserPermission.DOCTOR = "doctor";
    return IdentUserPermission;
}());
exports.IdentUserPermission = IdentUserPermission;
//# sourceMappingURL=userPermission.js.map