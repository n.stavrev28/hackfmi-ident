"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var vio_1 = require('vio');
var userPermission_1 = require("./userPermission");
var userDao_1 = require("../dao/userDao");
var IdentUserPermissionDescriptor = (function (_super) {
    __extends(IdentUserPermissionDescriptor, _super);
    function IdentUserPermissionDescriptor(name) {
        _super.call(this);
        this.name = name;
        this.name = name;
    }
    IdentUserPermissionDescriptor.prototype.validate = function (permission) {
        console.log(permission, this.name);
        return permission == this.name;
    };
    IdentUserPermissionDescriptor.PATIENT = new IdentUserPermissionDescriptor(userPermission_1.IdentUserPermission.PATIENT);
    IdentUserPermissionDescriptor.DOCTOR = new IdentUserPermissionDescriptor(userPermission_1.IdentUserPermission.DOCTOR);
    return IdentUserPermissionDescriptor;
}(vio_1.PermissionDescriptor));
exports.IdentUserPermissionDescriptor = IdentUserPermissionDescriptor;
var IdentUserProvider = (function () {
    function IdentUserProvider() {
        this._userDao = new userDao_1.UserDao();
    }
    IdentUserProvider.prototype.get = function (req) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this._userDao
                .findByUserName(req.session["loggedInUsername"])
                .then(function (user) {
                if (user) {
                    return resolve({
                        permission: user.permission
                    });
                }
                resolve(null);
            }, reject);
        });
    };
    // method `authenticate` instead of `get` would be invoked
    // if `authentication` in route options is true.
    IdentUserProvider.prototype.authenticate = function (req) {
        var _this = this;
        var userName = req.body["userName"];
        var password = req.body["password"];
        return new Promise(function (resolve, reject) {
            _this._userDao.findByUserNameAndPassword(userName, password)
                .then(function (user) {
                if (!user) {
                    return reject(new vio_1.ExpectedError(-1, "Invalid credentials"));
                }
                req.session["loggedInUsername"] = user.email;
                req.session["loggedInId"] = user._id;
                req.session.save(function (err) {
                    if (err) {
                        return reject(err);
                    }
                    resolve({
                        permission: user.permission
                    });
                });
            }, reject);
        });
    };
    return IdentUserProvider;
}());
exports.IdentUserProvider = IdentUserProvider;
//# sourceMappingURL=userProvider.js.map