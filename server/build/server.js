"use strict";
var path = require('path');
var express = require('express');
var session = require('express-session');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var vio_1 = require('vio');
var userProvider_1 = require("./security/userProvider");
mongoose.connect("mongodb://localhost:27017/ident");
var app = express();
app.use(bodyParser.json());
app.use(session({
    secret: "bashCodeIdentAppcation",
    resave: true,
    saveUninitialized: false,
    cookie: {
        maxAge: 1000 * 60 * 30
    },
    rolling: true
}));
var router = new vio_1.Router(app, {
    routesRoot: path.join(__dirname, 'routes'),
});
router.userProvider = new userProvider_1.IdentUserProvider();
app.listen(1337);
//# sourceMappingURL=server.js.map