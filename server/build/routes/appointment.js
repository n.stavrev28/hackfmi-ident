"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var vio_1 = require("vio");
var appointmentDao_1 = require("../dao/appointmentDao");
var default_1 = (function (_super) {
    __extends(default_1, _super);
    function default_1() {
        _super.call(this);
        this._appointmentDao = new appointmentDao_1.AppointmentDao();
    }
    default_1.prototype.findByDoctorId = function (req) {
        var doctorId = req.params["doctorId"];
        return this._appointmentDao
            .findByDoctorId(doctorId);
    };
    default_1.prototype.create = function (req) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this._appointmentDao
                .isFree(req.body)
                .then(function (isFree) {
                if (!isFree) {
                    return reject(new vio_1.ExpectedError(-1, "Appointment is not free"));
                }
                req.body.patient = req.session["loggedInId"];
                _this._appointmentDao
                    .save(req.body)
                    .then(resolve, reject);
            });
        });
    };
    __decorate([
        vio_1.get({
            path: "/doctor/:doctorId",
        })
    ], default_1.prototype, "findByDoctorId", null);
    __decorate([
        vio_1.post({
            path: "/",
        })
    ], default_1.prototype, "create", null);
    return default_1;
}(vio_1.Controller));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = default_1;
//# sourceMappingURL=appointment.js.map