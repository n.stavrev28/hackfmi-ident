"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var vio_1 = require("vio");
var userDao_1 = require("../dao/userDao");
var default_1 = (function (_super) {
    __extends(default_1, _super);
    function default_1() {
        _super.call(this);
        this._userDao = new userDao_1.UserDao();
    }
    default_1.prototype.findAll = function () {
        return this._userDao.findAll();
    };
    default_1.prototype.create = function (req) {
        return this._userDao.save(req.body);
    };
    __decorate([
        vio_1.get({
            path: "/"
        })
    ], default_1.prototype, "findAll", null);
    __decorate([
        vio_1.post({
            path: "/"
        })
    ], default_1.prototype, "create", null);
    return default_1;
}(vio_1.Controller));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = default_1;
//# sourceMappingURL=user.js.map