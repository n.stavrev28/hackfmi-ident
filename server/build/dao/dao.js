"use strict";
var Dao = (function () {
    function Dao(model) {
        this.model = model;
    }
    Dao.prototype.findAll = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.model.find({}, function (err, entities) {
                if (err) {
                    return reject(err);
                }
                resolve(entities);
            });
        });
    };
    Dao.prototype.findById = function (id) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.model.findById(id, function (err, entity) {
                if (err) {
                    return reject(err);
                }
                resolve(entity);
            });
        });
    };
    Dao.prototype.save = function (entity) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.model.create(entity, function (err, savedEntity) {
                if (err) {
                    return reject(err);
                }
                resolve(savedEntity);
            });
        });
    };
    Dao.prototype.update = function (newEntity, fields) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.findById(newEntity._id).then(function (entity) {
                if (!entity) {
                    return reject(new Error("entity not found"));
                }
                fields.forEach(function (field) {
                    entity.set(field, newEntity.get(field));
                });
                entity.save(function (err) {
                    if (err) {
                        return reject(err);
                    }
                    resolve(entity);
                });
            });
        });
    };
    return Dao;
}());
exports.Dao = Dao;
//# sourceMappingURL=dao.js.map