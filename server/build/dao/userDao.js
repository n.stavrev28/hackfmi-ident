"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var user_1 = require("../models/user");
var dao_1 = require("./dao");
var UserDao = (function (_super) {
    __extends(UserDao, _super);
    function UserDao() {
        _super.call(this, user_1.User);
    }
    UserDao.prototype.findByUserName = function (userName) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (!userName) {
                return resolve(null);
            }
            _this.model.findOne({
                email: userName.toString()
            }, function (err, user) {
                if (err) {
                    return reject(err);
                }
                resolve(user);
            });
        });
    };
    UserDao.prototype.findByUserNameAndPassword = function (userName, password) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (!userName) {
                return resolve(null);
            }
            _this.model.findOne({
                email: userName.toString(),
                password: password.toString()
            }, function (err, user) {
                if (err) {
                    return reject(err);
                }
                if (!user) {
                    return resolve(null);
                }
                if (user.password !== user.password) {
                    return resolve(null);
                }
                resolve(user);
            });
        });
    };
    return UserDao;
}(dao_1.Dao));
exports.UserDao = UserDao;
//# sourceMappingURL=userDao.js.map