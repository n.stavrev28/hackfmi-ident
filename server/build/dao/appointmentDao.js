"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var dao_1 = require("./dao");
var appointment_1 = require("../models/appointment");
var AppointmentDao = (function (_super) {
    __extends(AppointmentDao, _super);
    function AppointmentDao() {
        _super.call(this, appointment_1.Appointment);
    }
    AppointmentDao.prototype.findByDoctorId = function (doctorId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.model.find({
                doctor: doctorId
            }, function (err, appointments) {
                if (err) {
                    return reject(err);
                }
                resolve(appointments);
            });
        });
    };
    AppointmentDao.prototype.isFree = function (appointment) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.model.count({
                doctor: appointment.doctor,
                $or: [{
                        startDate: {
                            $lte: appointment.startDate
                        },
                        endDate: {
                            $gte: appointment.startDate
                        }
                    }, {
                        $and: [{
                                startDate: {
                                    $gte: appointment.startDate
                                }
                            }, {
                                startDate: {
                                    $lte: appointment.endDate
                                }
                            }]
                    }]
            }, function (err, appointments) {
                if (err) {
                    return reject(err);
                }
                resolve(appointments === 0);
            });
        });
    };
    return AppointmentDao;
}(dao_1.Dao));
exports.AppointmentDao = AppointmentDao;
//# sourceMappingURL=appointmentDao.js.map