"use strict";
var mongoose = require('mongoose');
exports.AppointmentSchema = new mongoose.Schema({
    startDate: {
        type: Date,
        required: true
    },
    endDate: {
        type: Date,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    patient: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user',
        required: true
    },
    doctor: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user',
        required: true
    }
});
// AppointmentSchema.pre("save",(next:Function) => {
//
//     _appointmentDao.isFree(this).then((isFree) => {
//
//     });
//
// })
exports.Appointment = mongoose.model("appointment", exports.AppointmentSchema);
//# sourceMappingURL=appointment.js.map