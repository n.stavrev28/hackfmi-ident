"use strict";
var mongoose = require('mongoose');
var userPermission_1 = require("../security/userPermission");
exports.UserSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    permission: {
        type: String,
        required: true,
        enum: [userPermission_1.IdentUserPermission.PATIENT, userPermission_1.IdentUserPermission.DOCTOR]
    }
});
// UserSchema.path("email").validate((email:any) => {
//     let emailRegex = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
//     return emailRegex.test(email);
// });
exports.User = mongoose.model("user", exports.UserSchema);
//# sourceMappingURL=user.js.map