import * as mongoose from 'mongoose';

import {Dao} from "./dao";
import {IAppointment, Appointment} from "../models/appointment";

export class AppointmentDao extends Dao<IAppointment, mongoose.Model<IAppointment>> {
    
    constructor(){
        super(Appointment)
    }

    findByDoctorId(doctorId:string){
        return new Promise<IAppointment[]>((resolve, reject) => {
           this.model.find({
               doctor : doctorId
           },(err:any, appointments:IAppointment[]) => {
               if(err){
                   return reject(err);
               }

               resolve(appointments);
           })
        });
    }

    isFree(appointment:IAppointment): Promise<Boolean>{
        return new Promise<Boolean>((resolve, reject) => {
            this.model.count({
                doctor : appointment.doctor,
                $or : [{
                    startDate : {
                        $lte : appointment.startDate
                    },
                    endDate : {
                        $gte : appointment.startDate
                    }
                }, {
                    $and : [{
                        startDate : {
                            $gte : appointment.startDate
                        }
                    }, {
                        startDate : {
                            $lte : appointment.endDate
                        }
                    }]
                }]
            },(err:any, appointments:number) => {
                if(err){
                    return reject(err);
                }

                resolve(appointments === 0);
            });
        });

    }
}