import * as mongoose from 'mongoose';

interface IDao<T> {
    findAll() : Promise<T[]>,
    findById(id: string) : Promise<T>,
    save(entity : T) : Promise<T>,
    update(entity:T, fields:String[]): Promise<T>
}

export abstract class Dao<T extends mongoose.Document, M extends mongoose.Model<T>> implements IDao<T>{

    protected model:M;

    constructor(model:M){
        this.model = model;
    }


    findAll():Promise<T[]> {
        return new Promise<T[]>((resolve, reject) =>{
            this.model.find({},(err:any, entities: T[]) =>{
                if(err){
                    return reject(err);
                }

                resolve(entities);
            });
        })
    }

    findById(id:string):Promise<T> {
        return new Promise<T>((resolve, reject) => {
            this.model.findById(id, (err:any, entity:T) => {
                if(err){
                    return reject(err);
                }

                resolve(entity);
            });
        });
    }

    save(entity:T):Promise<T> {
        return new Promise<T>((resolve, reject) => {
            this.model.create(entity, (err:any, savedEntity:T) => {
                if(err){
                    return reject(err);
                }

                resolve(savedEntity);
            });
        });
    }

    update(newEntity:T, fields:String[]):Promise<T> {
        return new Promise<T>((resolve, reject) => {

            this.findById(newEntity._id).then((entity:T) => {
                if(!entity){
                    return reject(new Error("entity not found"));
                }

                fields.forEach((field:string)=>{
                    entity.set(field, newEntity.get(field));
                });

                entity.save((err:any) => {
                    if(err){
                        return reject(err);
                    }

                    resolve(entity);
                });
            })

        });
    }


}