import * as mongoose from 'mongoose';
import {IUser, User} from "../models/user";
import {Dao} from "./dao";

export class UserDao extends Dao<IUser, mongoose.Model<IUser>> {

    constructor(){
        super(User)
    }

    findByUserName(userName:string):Promise<IUser>{
        return new Promise<IUser>((resolve, reject) => {
            if(!userName){
                return resolve(null);
            }

            this.model.findOne({
                email : userName.toString()
            }, (err:any, user:IUser) => {
                if(err) {
                    return reject(err);
                }

                resolve(user);
            })
        });
    }

    findByUserNameAndPassword(userName:string, password:string):Promise<IUser> {
        return new Promise<IUser>((resolve, reject) => {
            if(!userName){
                return resolve(null);
            }

            this.model.findOne({
                email : userName.toString(),
                password : password.toString()
            },(err:any, user:IUser) => {

                if(err) {
                    return reject(err);
                }

                if(!user){
                    return resolve(null);
                }

                if(user.password !== user.password){
                    return resolve(null)
                }

                resolve(user);
            });
        });
    }

}