import * as mongoose from 'mongoose';
import {IdentUserPermission} from "../security/userPermission";

export interface IUser extends mongoose.Document {
    email: string,
    password: string,
    permission: string
}

export const UserSchema = new mongoose.Schema({
    email : {
        type : String,
        required : true,
        unique : true
    },
    password : {
        type: String,
        required : true
    },
    permission : {
        type : String,
        required : true,
        enum: [IdentUserPermission.PATIENT, IdentUserPermission.DOCTOR]
    }
});

// UserSchema.path("email").validate((email:any) => {
//     let emailRegex = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
//     return emailRegex.test(email);
// });

export const User = mongoose.model<IUser>("user", UserSchema);