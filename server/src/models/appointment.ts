import * as mongoose from 'mongoose';
import {IUser} from "./user";

export interface IAppointment extends mongoose.Document {
    date: Date,
    user: IUser,
    startDate: Date,
    endDate: Date,
    description: string,
    patient: IUser,
    doctor: IUser
}

export const AppointmentSchema = new mongoose.Schema({
    startDate : {
        type : Date,
        required : true
    },
    endDate : {
        type : Date,
        required : true
    },
    description : {
        type : String,
        required : true
    },
    patient : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'user',
        required : true
    },
    doctor : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'user',
        required : true
    }
});

// AppointmentSchema.pre("save",(next:Function) => {
//
//     _appointmentDao.isFree(this).then((isFree) => {
//
//     });
//
// })

export const Appointment = mongoose.model<IAppointment>("appointment", AppointmentSchema)