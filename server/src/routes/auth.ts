import {Controller, get, post, ExpressRequest} from "vio";
import {UserDao} from "../dao/userDao";
import {IdentUserPermission} from "../security/userPermission";
import {IdentUserPermissionDescriptor} from "../security/userProvider";

export default class extends Controller{

    private _userDao:UserDao;

    constructor(){
        super();
        this._userDao = new UserDao();
    }

    @post({
        path : "/",
        authentication : true
    })
    authenticatePatient(req:ExpressRequest){
        return {
            authenticated : true
        }
    }


}