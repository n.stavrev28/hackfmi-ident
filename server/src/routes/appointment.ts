import {Controller, get, post, ExpressRequest, ExpectedError} from "vio";
import {AppointmentDao} from "../dao/appointmentDao";
import {IdentUserPermissionDescriptor} from "../security/userProvider";

export default class extends Controller{
    
    private _appointmentDao:AppointmentDao;
    
    constructor(){
        super();
        this._appointmentDao = new AppointmentDao();
    }

    @get({
        path : "/doctor/:doctorId",
    })
    findByDoctorId(req:ExpressRequest){
        let doctorId = req.params["doctorId"];

        return this._appointmentDao
            .findByDoctorId(doctorId);
    }

    @post({
        path : "/",
        // permissions : [IdentUserPermissionDescriptor.PATIENT]
    })
    create(req:ExpressRequest){
        return new Promise((resolve, reject) => {
            this._appointmentDao
                .isFree(req.body)
                .then((isFree:Boolean) => {
                    if(!isFree){
                        return reject(new ExpectedError(-1, "Appointment is not free"));
                    }

                    req.body.patient = req.session["loggedInId"];

                    this._appointmentDao
                        .save(req.body)
                        .then(resolve, reject);
                });
        });
    }

}