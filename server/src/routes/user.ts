import {Controller, get, post, ExpressRequest} from "vio";
import {UserDao} from "../dao/userDao";

export default class extends Controller{

    private _userDao:UserDao;

    constructor(){
        super();
        this._userDao = new UserDao();
    }

    @get({
        path : "/"
    })
    findAll(){
        return this._userDao.findAll();
    }

    @post({
        path : "/"
    })
    create(req:ExpressRequest){
        return this._userDao.save(req.body);
    }


}