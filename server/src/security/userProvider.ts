import {
    RequestUser,
    ExpressRequest,
    UserProvider,
    PermissionDescriptor, ExpectedError
} from 'vio';

import {IdentUserPermission} from "./userPermission";
import {UserDao} from "../dao/userDao";
import {IUser} from "../models/user";

export class IdentUserPermissionDescriptor extends PermissionDescriptor<IdentUserPermission> {

    constructor(public name:IdentUserPermission){
        super();
        this.name = name
    }

    validate(permission: IdentUserPermission): boolean {
        console.log(permission, this.name)
        return permission == this.name;
    }

    static PATIENT = new IdentUserPermissionDescriptor(IdentUserPermission.PATIENT);
    static DOCTOR = new IdentUserPermissionDescriptor(IdentUserPermission.DOCTOR)

}

export class IdentUserProvider implements UserProvider<RequestUser<IdentUserPermission>> {
    private _userDao:UserDao;

    constructor(){
        this._userDao = new UserDao();
    }

    get(req: ExpressRequest): Promise<RequestUser<IdentUserPermission>> {
        return new Promise<RequestUser<IdentUserPermission>>((resolve, reject) => {
            this._userDao
                .findByUserName(req.session["loggedInUsername"])
                .then((user:IUser) => {

                    if(user){
                        return resolve({
                            permission : user.permission
                        })
                    }
                    resolve(null);
                    
                }, reject)
        });
    }

    // method `authenticate` instead of `get` would be invoked
    // if `authentication` in route options is true.
    authenticate(req:ExpressRequest): Promise<RequestUser<IdentUserPermission>> {
        let userName = req.body["userName"];
        let password = req.body["password"];

        return new Promise<RequestUser<IdentUserPermission>>((resolve, reject) => {
            this._userDao.findByUserNameAndPassword(userName, password)
                .then((user) => {
                    if(!user){
                        return reject(new ExpectedError(-1, "Invalid credentials"));
                    }
                    req.session["loggedInUsername"] = user.email;
                    req.session["loggedInId"] = user._id;
                    req.session.save((err:any) => {
                        if(err){
                            return reject(err);
                        }

                        resolve({
                            permission : user.permission
                        });
                    });
                }, reject);
        });
    }

}
