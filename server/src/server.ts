import * as path from 'path';
import * as express from 'express';
import * as session from 'express-session';
import * as bodyParser from 'body-parser';
import * as mongoose from 'mongoose';

import {Router} from 'vio';
import {IdentUserProvider} from "./security/userProvider";

mongoose.connect("mongodb://localhost:27017/ident")

let app = express();

app.use(bodyParser.json());

app.use(session({
    secret : "bashCodeIdentAppcation",
    resave : true,
    saveUninitialized : false,
    cookie : {
        maxAge : 1000*60*30
    },
    rolling : true
}))

let router = new Router(app, {
    routesRoot: path.join(__dirname, 'routes'),
});

router.userProvider = new IdentUserProvider();

app.listen(1337);